var gulp = require('gulp'),
    // Prepare and optimize code etc
    autoprefixer = require('autoprefixer'),
    browserSync = require('browser-sync').create(),
    postcss = require('gulp-postcss'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),

    // Only work with new or updated files
    newer = require('gulp-newer'),

    // Name of working theme folder
    admin_scss = 'sass/topper-admin.scss',
    public_scss = 'sass/topper-public.scss',
    admin_css = '../admin/css',
    public_css = '../public/css/';

gulp.task('browser-sync', function(){
    browserSync.init({
        open: 'external',
        proxy: 'wp.dev',
        port: 8080
    });
});

gulp.task('admin-css', function () {
    return gulp.src(admin_scss)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            indentType: 'tab',
            indentWidth: '1'
        }).on('error', sass.logError))
        .pipe(postcss([
            autoprefixer('last 2 versions', '> 1%')
        ]))
        .pipe(sourcemaps.write(''))
        .pipe(gulp.dest(admin_css))
        .pipe(browserSync.stream({match: '**/*.css'}));
});

gulp.task('public-css', function () {
    return gulp.src(public_scss)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            indentType: 'tab',
            indentWidth: '1'
        }).on('error', sass.logError))
        .pipe(postcss([
            autoprefixer('last 2 versions', '> 1%')
        ]))
        .pipe(sourcemaps.write(''))
        .pipe(gulp.dest(public_css))
        .pipe(browserSync.stream({match: '**/*.css'}));
});

gulp.task('watch', ['admin-css', 'public-css', 'browser-sync'] , function () {
    gulp.watch([admin_scss], ['admin-css']);
    gulp.watch([public_scss], ['public-css']);
    gulp.watch(['../**/*.php', '../**/*.html', '../**/*.js' ], browserSync.reload);
});

gulp.task('css', ['admin-css', 'public-css'] , function () {
    gulp.watch([admin_scss], ['admin-css']);
    gulp.watch([public_scss], ['public-css']);
    gulp.watch(['../**/*.php', '../**/*.html', '../**/*.js' ], browserSync.reload);
});

// Default task (runs at initiation: gulp --verbose)
gulp.task('default', ['watch']);
