jQuery(document).ready(function ($) {

    $('.wysiwyg-textarea').each(function (index, element) {
        var is_modal = $(element).attr('name') === 'banner_content';

        $(element).wysiwyg({
            class: '',
            toolbar: 'top' | 'selection',
            buttons: {
                // banner_btn: is_modal === true ? false : {
                // title: 'Кнопка',
                // image: '\uf14c',
                // html: '&lt;raw html&gt;',
                // //         // What should the button do?
                // popup: function ($popup, $button) {
                //     $popup.append('<div></div>')
                //
                // },
                //         click: function( $button ) { },
                //         // Where should the button be placed?
                //         showstatic: true, // on the static toolbar
                //         showselection: true // on selection toolbar
                //         // anything else passed as "prop()" ...
                // },
                // build-in:
                insertlink: {
                    title: 'Insert link',
                    image: '\uf0c1'
                },
                bold: {
                    title: 'Bold (Ctrl+B)',
                    image: '\uf032',
                    hotkey: 'b'
                },
                italic: {
                    title: 'Italic (Ctrl+I)',
                    image: '\uf033',
                    hotkey: 'i'
                },
                underline: {
                    title: 'Underline (Ctrl+U)',
                    image: '\uf0cd',
                    hotkey: 'u'
                },
                strikethrough: {
                    title: 'Strikethrough (Ctrl+S)',
                    image: '\uf0cc',
                    hotkey: 's'
                },
                forecolor: {
                    title: 'Text color',
                    image: '\uf1fc'
                },
                highlight: {
                    title: 'Background color',
                    image: '\uf043'
                },
                fontsize: {
                    title: 'Size',
                    style: 'color:white;background:red',      // you can pass any property - example: "style"
                    image: '\uf034',
                    popup: function ($popup, $button) {
                        var list_fontsizes = [];
                        for (var i = 8; i <= 11; ++i)
                            list_fontsizes.push(i + 'px');
                        for (var i = 12; i <= 28; i += 2)
                            list_fontsizes.push(i + 'px');
                        list_fontsizes.push('36px');
                        list_fontsizes.push('48px');
                        list_fontsizes.push('72px');
                        var $list = $('<div/>').addClass('wysiwyg-plugin-list')
                            .attr('unselectable', 'on');
                        $.each(list_fontsizes, function (index, size) {
                            var $link = $('<a/>').attr('href', '#')
                                .html(size)
                                .click(function (event) {
                                    $(element).wysiwyg('shell').fontSize(7).closePopup();
                                    $(element).wysiwyg('container')
                                        .find('font[size=7]')
                                        .removeAttr("size")
                                        .css("font-size", size);
                                    // prevent link-href-#
                                    event.stopPropagation();
                                    event.preventDefault();
                                    return false;
                                });
                            $list.append($link);
                        });
                        $popup.append($list);
                    }
                    //showstatic: true,    // wanted on the toolbar
                    //showselection: true    // wanted on selection
                },
                alignleft: {
                    title: 'Left',
                    image: '\uf036',
                    showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                aligncenter: {
                    title: 'Center',
                    image: '\uf037',
                    showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                alignright: {
                    title: 'Right',
                    image: '\uf038',
                    showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                alignjustify: {
                    title: 'Justify',
                    image: '\uf039',
                    showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                subscript: {
                    title: 'Subscript',
                    image: '\uf12c',
                    showstatic: true,    // wanted on the toolbar
                    showselection: true    // wanted on selection
                },
                superscript: {
                    title: 'Superscript',
                    image: '\uf12b',
                    showstatic: true,    // wanted on the toolbar
                    showselection: true    // wanted on selection
                },
                indent: is_modal === true ? false : {
                    title: 'Indent',
                    image: '\uf03c',
                    // showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                outdent: is_modal === true ? false : {
                    title: 'Outdent',
                    image: '\uf03b',
                    // showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                removeformat: {
                    title: 'Remove format',
                    image: '\uf12d'
                }
            }
            // submit: {  },
            // selectImage: 'Click or drop image',
            // placeholderUrl: 'www.example.com',
            // placeholderEmbed: '&lt;embed/&gt;',
            // maxImageSize: [600,200],
            // filterImageType: function( file ) {  },
            // onKeyDown: function( key, character, shiftKey, altKey, ctrlKey, metaKey ) {  },
            // onKeyPress: function( key, character, shiftKey, altKey, ctrlKey, metaKey ) {  },
            // onKeyUp: function( key, character, shiftKey, altKey, ctrlKey, metaKey ) {  },
            // onAutocomplete: function( tyed, key, character, shiftKey, altKey, ctrlKey, metaKey ) {  },
            // onImageUpload: function( insert_image ) {  },
            // forceImageUpload: false,
            // videoFromUrl: function( url ) {  }
        });
    });


    $('button.save-banner+span').hide();
    /**
     * Создает новый баннер и перезагружает страницу
     */
    $('.add_topper').on('click', function (event) {
        event.preventDefault();
        var data = {
            action: 'add_topper'
        };
        jQuery.post(ajaxurl, data, function () {
            location.reload();
        });
    });

    /**
     * Удаляет баннер без перезагрузки страницы
     */
    $('.admin-banner-container').on('click', '.remove-banner', function (event) {
        event.preventDefault();
        var id = $(this).attr('data-banner-id');
        var data = {
            action: 'remove_banner',
            banner_id: id
        };
        jQuery.post(ajaxurl, data, function (response) {

            var $topper_accordion = $('#toppers-accordion');
            $(response).remove(response);
            $topper_accordion.accordion('refresh').accordion("option", "active", -1);

        });
    });

    /**
     * Сохраняет параметры баннера
     */
    $('.save-banner').on('click', function (event) {
        event.preventDefault();
        var id = $(this).attr('data-banner-id');
        var $banner = $("form[name='banner-" + id + "']");
        var banner_name = $banner.find('input[name="banner-name"]').val();
        var banner_content = $banner.find('textarea[name="banner-content"]').val();
        var banner_hide_xs = '';
        var banner_hide_sm = '';
        var banner_hide_md = '';
        var banner_hide_lg = '';
        var banner_bg_color = $banner.find('input[name="banner-bg-color"]').val();
        var banner_display_pages = '';
        var banner_display_cat = '';
        var banner_display_home = '';
        var banner_hide_btn = '';
        var banner_timer_to_show = $banner.find('input[name="timer-to-show"]').val();

        $banner.find('input[name="page"],input[name="post"]').each(function () {
            if ($(this).prop('checked')) {
                banner_display_pages += $(this).val() + ',';
            }
        });

        banner_display_pages = banner_display_pages.replace(/^,|,$/g, '');
        var arr = $.unique(banner_display_pages.split(','));
        banner_display_pages = arr.join(",");

        $banner.find('input[name="category_description"]').each(function () {
            if ($(this).prop('checked')) {
                banner_display_cat += $(this).val() + ',';
            }
        });

        banner_display_cat = banner_display_cat.replace(/^,|,$/g, '');
        arr = $.unique(banner_display_cat.split(','));
        banner_display_cat = arr.join(",");

        if ($banner.find('input[name="hide-xs"]').prop('checked'))
            banner_hide_xs = 'hide-xs';
        if ($banner.find('input[name="hide-sm"]').prop('checked'))
            banner_hide_sm = 'hide-sm';
        if ($banner.find('input[name="hide-md"]').prop('checked'))
            banner_hide_md = 'hide-md';
        if ($banner.find('input[name="hide-lg"]').prop('checked'))
            banner_hide_lg = 'hide-lg';

        if ($banner.find('input[name="hide-btn"]').prop('checked'))
            banner_hide_btn = 'true';

        if ($banner.find('input[name="home"]').prop('checked'))
            banner_display_home = 'true';

        var data = {
            action: 'save_banner',
            banner_id: id,
            banner_name: banner_name,
            banner_content: banner_content,
            banner_hide_xs: banner_hide_xs,
            banner_hide_sm: banner_hide_sm,
            banner_hide_md: banner_hide_md,
            banner_hide_lg: banner_hide_lg,
            banner_bg_color: banner_bg_color,
            banner_display_pages: banner_display_pages,
            banner_display_cat: banner_display_cat,
            banner_display_home: banner_display_home,
            banner_hide_btn: banner_hide_btn,
            banner_timer_to_show: banner_timer_to_show


        };
        jQuery.post(ajaxurl, data, function (response) {

            var $save_state = $banner.find('button.save-banner+span');

            $save_state.css({'color': 'green'});

            $save_state.fadeIn(500);
            $save_state.fadeOut(2000);

            if ( 'true' === response)
                alert('Внимание! Не удалось автоматически очистить кеш. Очистите его вручную.');

        });

    });

    /**
     * Формирует аккордион из баннеров
     */
    $("#toppers-accordion").accordion({
        header: "h3",
        active: -1,
        collapsible: true,
        beforeActivate: function (event, ui) {
            // The accordion believes a panel is being opened
            var currHeader;
            var currContent;
            if (ui.newHeader[0]) {
                currHeader = ui.newHeader;
                currContent = currHeader.next('.ui-accordion-content');
                // The accordion believes a panel is being closed
            } else {
                currHeader = ui.oldHeader;
                currContent = currHeader.next('.ui-accordion-content');
            }
            // Since we've changed the default behavior, this detects the actual status
            var isPanelSelected = currHeader.attr('aria-selected') === 'true';

            // Toggle the panel's header
            currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));

            // Toggle the panel's icon
            currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);

            // Toggle the panel's content
            currContent.toggleClass('accordion-content-active', !isPanelSelected);
            if (isPanelSelected) {
                currContent.slideUp();
            } else {
                currContent.slideDown();
            }

            return false; // Cancels the default action
        }
    });

    $("input[name='all-pages']").on('click', function () {
        var display_widget = $(this).parents('.display-widgets');
        var isAllChecked = $(this).prop('checked');
        display_widget.find("input.uk-checkbox").prop('checked', isAllChecked);
    });

    $("input[name='category']").on('click', function () {
        var id = $(this).val();
        var isCatChecked = $(this).prop('checked');
        $('.posts-from-category-' + id + ' input').prop('checked', isCatChecked);
    });

    $("input[name='post']").on('click', function () {
        var banner = $(this).parents('.admin-banner-container');
        var post_id = $(this).val();
        var isChecked = $(this).prop('checked');
        banner.find("input.post-" + post_id).prop('checked', isChecked);
    });

    $(".choose_template").on('click', function () {
        var id = $(this).attr('data-banner-id');
        $("#template_modal_content-" + id).wysiwyg('shell').setHTML('');
    });

    $(".template_save").on('click', function (event) {
        event.preventDefault();
        var $template_modal = $(this).parents('.template_modal');
        var banner_id = $template_modal.attr("id").replace("template_modal_id-", "");
        var $banner = $("form[name='banner-" + banner_id + "']");
        var $banner_content = $banner.find("textarea.banner-content");
        var $banner_content_formatted = $banner.find("textarea.banner-content-formatted");
        var content_value = $template_modal.find("textarea[name='banner_content']").val();
        var content_btn = $template_modal.find("input[name='btn_content']").val();
        var content_btn_link = $template_modal.find("input[name='btn_link']").val();
        var content_btn_bg_color = $template_modal.find("input[name='btn_bg_color']").val();
        var content_btn_text_color = $template_modal.find("input[name='btn_text_color']").val();
        var content = '<style>'
            + '.topper_table a.btn:hover { color:' + content_btn_text_color + ' !important;}'
            + '</style>'
            + '<div class="topper_table">'
            + '<div class="topper_table_row">'
            + '<div class="topper_table_cell"><p>'
            + content_value
            + '</p></div>'
            + '<div class="topper_table_cell">'
            + '<a href="' + content_btn_link + '" class="btn btn-3 btn-3e icon-mail" style="background-color:' + content_btn_bg_color
            + '; color: ' + content_btn_text_color + '; " target="_blank">'
            + content_btn
            + '</a></div></div></div>';

        $banner_content.val(content);
        $banner_content_formatted.wysiwyg('shell').setHTML(content);
    });

    $('.banner-content').on('change', function () {
        var id = $(this).attr('id').replace('banner_content-', '');
        var content_to_sync = $(this).val();
        $('#banner_content-formatted-' + id).wysiwyg('shell').setHTML(content_to_sync);
    });

    $('.banner-content-formatted').on('change', function () {
        var id = $(this).attr('id').replace('banner_content-formatted-', '');
        var content_to_sync = $(this).wysiwyg('shell').getHTML();
        $('#banner_content-' + id).val(content_to_sync);
    });


});
