<?php
/**
 * @internal never define functions inside callbacks.
 * these functions could be run multiple times; this would result in a fatal error.
 */


/**
 * @param string $hook_suffix Возвращает название текущей страницы
 */
function topper_admin_enqueue_scripts_and_styles( $hook_suffix ) {

	if ( 'toplevel_page_topper' === $hook_suffix ) {
		wp_dequeue_style( 'forms' );
		wp_deregister_style( 'forms' );

		wp_enqueue_style(
			'font-awesome',
			'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css',
			array(),
			'4.4.0',
			'all'
		);

		wp_enqueue_style(
			'jquery-ui-base',
			'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
			array(),
			'1.12.1',
			'all'
		);

		wp_enqueue_style(
			'forms',
			'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.25/css/uikit.min.css',
			array(),
			'3.0.0-beta.25',
			'all'
		);

		wp_enqueue_style(
			'topper-admin-style',
			plugin_dir_url( __FILE__ ) . 'css/topper-admin.css',
			array( 'jquery-ui-base' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_script(
			'uikit-js',
			'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.25/js/uikit.min.js',
			array( 'jquery' ),
			'3.0.0-beta.25',
			true
		);

		wp_enqueue_script(
			'uikit-icons-js',
			'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.25/js/uikit-icons.min.js',
			array( 'uikit-js' ),
			'3.0.0-beta.25',
			true
		);

		wp_enqueue_script(
			'topper-admin-js',
			plugin_dir_url( __FILE__ ) . 'js/topper-admin.js',
			array( 'jquery-ui-accordion', 'wysiwyg-editor-js' ),
			'1.0.0',
			true
		);

		wp_enqueue_style(
			'wysiwyg-style',
			plugin_dir_url( __FILE__ ) . 'libs/wysiwyg.js/wysiwyg-editor.min.css',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_script(
			'wysiwyg-js',
			plugin_dir_url( __FILE__ ) . 'libs/wysiwyg.js/wysiwyg.min.js',
			array( 'jquery' ),
			'1.0.0',
			true
		);

		wp_enqueue_script(
			'wysiwyg-editor-js',
			plugin_dir_url( __FILE__ ) . 'libs/wysiwyg.js/wysiwyg-editor.min.js',
			array( 'wysiwyg-js' ),
			'1.0.0',
			true
		);

	}

}

add_action( 'admin_enqueue_scripts', 'topper_admin_enqueue_scripts_and_styles' );


/**
 * Создает баннер по ajax запросу
 */
function ajax_topper_add_topper_callback() {
	$topper = new Topper();
	$banner = $topper->add_new_banner();

	echo $banner->render_admin();

	wp_die();
}

add_action( 'wp_ajax_add_topper', 'ajax_topper_add_topper_callback' );

/**
 * Удаляет баннер по ajax запросу
 */
function ajax_topper_remove_banner_callback() {
	$topper    = new Topper();
	$banner_id = intval( $_POST['banner_id'] );

	echo ".banner-{$banner_id}";

	$topper->remove_banner( $banner_id );

	wp_die();
}

add_action( 'wp_ajax_remove_banner', 'ajax_topper_remove_banner_callback' );

/**
 * Сохраняет настройки баннера по ajax зпросу
 */
function ajax_topper_save_banner_callback() {
	$banner_id            = intval( $_POST['banner_id'] );
	$banner_name          = strval( $_POST['banner_name'] );
	$banner_content       = strval( $_POST['banner_content'] );
	$banner_hide_xs       = strval( $_POST['banner_hide_xs'] );
	$banner_hide_sm       = strval( $_POST['banner_hide_sm'] );
	$banner_hide_md       = strval( $_POST['banner_hide_md'] );
	$banner_hide_lg       = strval( $_POST['banner_hide_lg'] );
	$banner_bg_color      = strval( $_POST['banner_bg_color'] );
	$banner_display_pages = strval( $_POST['banner_display_pages'] );
	$banner_display_cat   = strval( $_POST['banner_display_cat'] );
	$banner_display_home  = strval( $_POST['banner_display_home'] );
	$banner_hide_btn      = strval( $_POST['banner_hide_btn'] );
	$banner_timer_to_show = intval( $_POST['banner_timer_to_show'] );


	$topper = new Topper();
	$topper->remove_banner( $banner_id );
	$new_banner = $topper->add_new_banner();

	$banner_hide_opt = ' ' . $banner_hide_xs . ' ' . $banner_hide_sm . ' ' . $banner_hide_md . ' ' . $banner_hide_lg . ' ';

	$new_banner->set_banner_name( $banner_name );
	$new_banner->set_banner_content( $banner_content );
	$new_banner->set_banner_hide_opt( $banner_hide_opt );
	$new_banner->set_banner_bg_color( $banner_bg_color );
	$new_banner->set_banner_display_pages( $banner_display_pages );
	$new_banner->set_banner_display_cat( $banner_display_cat );
	$new_banner->set_banner_display_home( $banner_display_home );
	$new_banner->set_banner_hide_btn( $banner_hide_btn );
	$new_banner->set_banner_timer_to_show( $banner_timer_to_show );

	$new_banner->save_banner();

	if ( isset( $GLOBALS['wp_fastest_cache'] ) && method_exists( $GLOBALS['wp_fastest_cache'], 'deleteCache' ) ) {
		$GLOBALS['wp_fastest_cache']->deleteCache( true );
		echo 'true';
	} else {
		echo 'false';
	}


	wp_die();
}

add_action( 'wp_ajax_save_banner', 'ajax_topper_save_banner_callback' );


/**
 * top level menu
 */
function topper_options_page() {
	// add top level menu page
	add_menu_page(
		'Topper',
		'Topper Options',
		'manage_options',
		'topper',
		'topper_options_page_html'
	);
}

/**
 * register our topper_options_page to the admin_menu action hook
 */
add_action( 'admin_menu', 'topper_options_page' );

/**
 * top level menu:
 * callback functions
 */
function topper_options_page_html() {
	$topper = new Topper();

	// check user capabilities
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}

	// add error/update messages

	// check if the user have submitted the settings
	// wordpress will add the "settings-updated" $_GET parameter to the url
	if ( isset( $_GET['settings-updated'] ) ) {
		// add settings saved message with the class of "updated"
		add_settings_error( 'topper_messages', 'topper_message', __( 'Settings Saved', 'topper' ), 'updated' );
	}

	// show error/update messages
	settings_errors( 'topper_messages' );
	?>
	<div class="wrap">

		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<div id="toppers">
			<div id="toppers-accordion">
				<?php

				$topper->render_all();

				?>

			</div>

		</div>
		<br>
		<button class="add_topper uk-button uk-button-primary uk-button-small">Создать баннер</button>
		<br>

	</div>
	<?php
}
