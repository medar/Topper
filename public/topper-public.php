<?php

function topper_public_enqueue_scripts_and_styles() {

	wp_enqueue_script(
		'js-cookie',
		'https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js',
		array(),
		'2.1.4',
		true
	);

	wp_enqueue_style(
		'font-awesome',
		'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
		array(),
		'4.7.0',
		'all'
	);

	wp_enqueue_style(
		'buttons-components',
		plugin_dir_url( __FILE__ ) . 'libs/creative-buttons/css/component.css',
		array( 'font-awesome' ),
		'1.0.0',
		'all'
	);
	wp_enqueue_style(
		'topper-public-style',
		plugin_dir_url( __FILE__ ) . 'css/topper-public.css',
		array(),
		'1.0.0',
		'all'
	);

	wp_enqueue_script(
		'topper-public-js',
		plugin_dir_url( __FILE__ ) . 'js/topper-public.js',
		array( 'jquery' ),
		'1.0.1',
		true
	);
}

add_action( 'wp_enqueue_scripts', 'topper_public_enqueue_scripts_and_styles' );


function topper_public_widget() {
	$topper  = new Topper();
	$page_id = get_the_ID();
	$cat_id  = get_query_var( 'cat' );
	$banner  = new TopperBanner( $topper->get_current_banner( is_front_page(), is_category(), $page_id, $cat_id ) );
	if ( ! is_null( $banner->get_banner_content() ) ) :
		echo $banner->render_public();
	endif;

}

add_action( 'wp_footer', 'topper_public_widget' );
