jQuery(document).ready(function ($) {
    var $topper = $(".wrap-topper-top");
    var banner_id = $topper.attr('id');
    var banner_cookie_visibility = banner_id + '_visibility';
    var $topper_hide_btn = $(".topper-hide-btn");
    var topper_height = $topper.height();
    var topper_timer_to_show = $topper.attr("data-timer-to-show") * 1000;
    var wpadminbar_height = 0;
    var topper_interval = 700;

    function topper_click_hide() {
      Cookies.set(banner_cookie_visibility, 'false', { expires: 7 });
      $topper.animate({height: 0}, topper_interval);
      $topper_hide_btn.animate({top: wpadminbar_height}, topper_interval);
      $topper_hide_btn.find('i').removeClass("fa-angle-up");
      $topper_hide_btn.find('i').addClass("fa-angle-down");
      $("html").animate({"padding-top": 0}, topper_interval);
    }

    function topper_click_show() {
      Cookies.set(banner_cookie_visibility, 'true', { expires: 7 });
      $topper.animate({height: topper_height}, topper_interval);
      $topper_hide_btn.animate({top: wpadminbar_height + topper_height}, topper_interval);
      $topper_hide_btn.find('i').removeClass("fa-angle-down");
      $topper_hide_btn.find('i').addClass("fa-angle-up");
      $("html").animate({"padding-top": topper_height}, topper_interval);
    }

    function init_banner() {
      var visibility = Cookies.get(banner_cookie_visibility);
      if ( visibility == null ) {
        Cookies.set(banner_cookie_visibility, 'true', { expires: 7 });
      }
    }

    function run_banner() {
      if ( Cookies.get(banner_cookie_visibility) === 'true' ) {
        $topper.css({height: 0, top: wpadminbar_height});
        $topper_hide_btn.css({top: wpadminbar_height});
        $topper.show();
        $topper_hide_btn.show();
        $topper.animate({height: topper_height}, topper_interval);
        $topper_hide_btn.animate({top: wpadminbar_height + topper_height}, topper_interval);
        $("html").animate({"padding-top": topper_height}, topper_interval);

        $topper_hide_btn.toggle(function() {
            topper_click_hide();
        }, function(){
            topper_click_show();
        });
      } else {
        $topper.css({height: 0, top: wpadminbar_height});
        $topper_hide_btn.css({top: wpadminbar_height});
        $topper.show();
        $topper_hide_btn.show();

        $topper_hide_btn.find('i').removeClass("fa-angle-up");
        $topper_hide_btn.find('i').addClass("fa-angle-down");

        $topper_hide_btn.toggle(function() {
            topper_click_show();
        }, function(){
            topper_click_hide();
        });
      }
    }

    init_banner();
    setTimeout(run_banner, topper_timer_to_show);
});
