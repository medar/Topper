<?php
/**
 * Plugin Name: Topper
 * Version: 1.0.4
 * Author: Max Medar
 * License: GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: topper
 * Domain Path: /languages
 *
 * Topper is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * Topper is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Topper. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function topper_install() {

	topper_settings_init();

}

register_activation_hook( __FILE__, 'topper_install' );

function topper_deactivation() {

}

register_deactivation_hook( __FILE__, 'topper_deactivation' );

/**
 * Проверяет наличие настроек
 *
 * @return bool
 */
function topper_settings_exist() {
	if ( ! get_option( 'topper_options' ) ) {
		return false;
	}

	return true;
}

/**
 * Создает начальные настройки плагина
 */
function topper_settings_init() {

	add_action( 'admin_init', 'topper_settings_init' );
}


require_once( dirname( __FILE__ ) . '/includes/class-topper.php' );
require_once( dirname( __FILE__ ) . '/includes/class-topperbanner.php' );
require_once( dirname( __FILE__ ) . '/includes/class-topperbanneroptions.php' );

if ( is_admin() ) {
//  we are in admin mode
	require_once( dirname( __FILE__ ) . '/admin/topper-admin.php' );

} else {
	require_once( dirname( __FILE__ ) . '/public/topper-public.php' );
}



