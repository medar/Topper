<?php


class Topper {

	private $options;
	private $ids;

	public function __construct() {
		$options = $this->init_options();
		if ( isset( $options ) ) {
			$this->options = $options;
			$this->ids     = $this->options->ids;
		}

	}

	private function init_options() {
		$topper_options = get_option( 'topper_options', null );
		$topper_options = json_decode( $topper_options );

		return $topper_options;
	}

	private function set_new_id( $new_id ) {
		$this->ids[]        = $new_id;
		$this->options->ids = $this->ids;
		update_option( 'topper_options', wp_json_encode( $this->options ) );
	}

	private function get_new_id() {
		$date = new DateTime();

		return $date->getTimestamp();
	}

	public function add_new_banner() {
		$new_id = $this->get_new_id();
		$this->set_new_id( $new_id );
		$banner = new TopperBanner( $new_id );

		return $banner;
	}

	/**
	 *
	 */
	public function render_all() {

		if ( isset( $this->ids ) ) {
			foreach ( $this->ids as $id ) {
				$banner = new TopperBanner( $id );
				echo $banner->render_admin();
			}
			unset( $id );
		}
	}

	/**
	 * @param $id
	 */
	public function remove_banner( $id ) {
		array_splice( $this->ids, array_search( $id, $this->ids, true ), 1 );
		$this->options->ids = $this->ids;
		update_option( 'topper_options', wp_json_encode( $this->options ) );
		delete_option( "topper_banner-{$id}" );
	}

	public function get_current_banner( $is_home, $is_cat, $post_id, $cat_id ) {

		foreach ( $this->ids as $id ) {
			$banner = new TopperBanner( $id );
			if ( $is_home ) {
				if ( $banner->get_banner_display_home() ) {
					return $id;
				}
			} elseif ( $is_cat ) {
				if ( in_array( $cat_id, $banner->get_banner_display_cat() ) ) {
					return $id;
				}
			} else {
				if ( in_array( $post_id, $banner->get_banner_display_pages() ) ) {
					return $id;
				}
			}
		}

		return false;
	}

}
