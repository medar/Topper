<?php

class TopperBannerOptions {
	public $id;
	public $name = 'Новый баннер';
	public $content;
	public $hide_opt;
	public $bg_color;
	public $display_pages;
	public $display_cat;
	public $display_home;
	public $hide_btn = false;
	public $timer_to_show;

	/**
	 * TopperBannerOptions constructor.
	 *
	 * @param $id
	 */
	public function __construct( $id ) {
		$this->id = $id;
		$options  = get_option( "topper_banner-{$id}", null );
		$options  = json_decode( $options );
		if ( isset( $options->name ) ) {
			$this->name = $options->name;
		}
		if ( isset( $options->content ) ) {
			$this->content = $options->content;
		}
		if ( isset( $options->hide_opt ) ) {
			$this->hide_opt = $options->hide_opt;
		}
		if ( isset( $options->bg_color ) ) {
			$this->bg_color = $options->bg_color;
		}
		if ( isset( $options->display_pages ) ) {
			$this->display_pages = $options->display_pages;
		}
		if ( isset( $options->display_cat ) ) {
			$this->display_cat = $options->display_cat;
		}
		if ( isset( $options->display_home ) ) {
			$this->display_home = $options->display_home;
		}
		if ( isset( $options->hide_btn ) ) {
			$this->hide_btn = $options->hide_btn;
		}
		if ( isset( $options->timer_to_show ) ) {
			$this->timer_to_show = $options->timer_to_show;
		}
	}

	/**
	 * @return mixed
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function get_name() {
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function get_content() {
		return $this->content;
	}

	/**
	 * @return string
	 */
	public function get_hide_opt() {
		return $this->hide_opt;
	}

	/**
	 * @return string
	 */
	public function get_bg_color() {
		return $this->bg_color;
	}

	/**
	 * @return mixed
	 */
	public function get_display_pages() {
		return $this->display_pages;
	}

	/**
	 * @return mixed
	 */
	public function get_display_cat() {
		return $this->display_cat;
	}

	public function get_display_home() {
		return $this->display_home;
	}

	public function get_hide_btn() {
		return $this->hide_btn;
	}

	public function get_timer_to_show() {
		return $this->timer_to_show;
	}


	/**
	 * @param $new_name
	 */
	public function set_name( $new_name ) {
		$this->name = $new_name;
	}

	/**
	 * @param $new_content
	 */
	public function set_content( $new_content ) {
		$this->content = $new_content;
	}

	/**
	 * @param $new_hide_opt
	 */
	public function set_hide_opt( $new_hide_opt ) {
		$this->hide_opt = $new_hide_opt;
	}

	/**
	 * @param $new_bg_color
	 */
	public function set_bg_color( $new_bg_color ) {
		$this->bg_color = $new_bg_color;
	}

	/**
	 * @param $new_display_pages
	 */
	public function set_display_pages( $new_display_pages ) {
		$this->display_pages = $new_display_pages;
	}

	public function set_display_cat( $new_display_cat ) {
		$this->display_cat = $new_display_cat;
	}

	public function set_display_home( $new_display_home ) {
		$this->display_home = $new_display_home;
	}

	public function set_hide_btn( $new_hide_btn ) {
		$this->hide_btn = $new_hide_btn;
	}

	public function set_timer_to_show( $new_timer_to_show ) {
		$this->timer_to_show = $new_timer_to_show;
	}
}
