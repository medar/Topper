<?php


class TopperBanner {

	private $id;
	private $name;
	private $content;
	private $hide_opt;
	private $bg_color;
	private $display_pages;
	private $display_cat;
	private $display_home;
	private $hide_btn;
	private $timer_to_show;
	private $options;

	/**
	 * @param $id
	 */
	private function settings_init( $id ) {
		$this->options       = new TopperBannerOptions( $id );
		$this->id            = $id;
		$this->name          = $this->options->get_name();
		$this->content       = $this->options->get_content();
		$this->hide_opt      = $this->options->get_hide_opt();
		$this->bg_color      = $this->options->get_bg_color();
		$this->display_pages = $this->options->get_display_pages();
		$this->display_cat   = $this->options->get_display_cat();
		$this->display_home  = $this->options->get_display_home();
		$this->hide_btn      = $this->options->get_hide_btn();
		$this->hide_btn      = $this->options->get_hide_btn();
		$this->timer_to_show = $this->options->get_timer_to_show();
	}

	/**
	 * TopperBanner constructor.
	 *
	 * @param $id
	 */
	public function __construct( $id ) {
		$this->settings_init( $id );
		$this->save_options();
	}

	public function save_options() {
		update_option( "topper_banner-{$this->id}", wp_json_encode( $this->options ) );
	}

	/**
	 * @return mixed
	 */
	public function get_banner_id() {
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function get_banner_name() {
		return $this->name;
	}

	/**
	 * @return mixed
	 */
	public function get_banner_content() {
		return $this->content;
	}

	/**
	 * @return mixed
	 */
	public function get_banner_hide_opt() {
		return $this->hide_opt;
	}

	/**
	 * @return mixed
	 */
	public function get_banner_bg_color() {
		return $this->bg_color;
	}

	/**
	 * @return array
	 */
	public function get_banner_display_pages() {
		$display_pages = explode( ',', $this->display_pages );

		return $display_pages;
	}

	/**
	 * @return array
	 */
	public function get_banner_display_cat() {
		$display_cat = explode( ',', $this->display_cat );

		return $display_cat;
	}

	public function get_banner_display_home() {
		return $this->display_home;
	}

	public function get_banner_hide_btn() {
		return $this->hide_btn;
	}

	public function get_timer_to_show() {
		return $this->timer_to_show;
	}


	/**
	 * @param $new_name
	 */
	public function set_banner_name( $new_name ) {
		$this->name = $new_name;
	}

	/**
	 * @param $new_content
	 */
	public function set_banner_content( $new_content ) {
		$this->content = $new_content;
	}

	/**
	 * @param $new_hide_opt
	 */
	public function set_banner_hide_opt( $new_hide_opt ) {
		$this->hide_opt = $new_hide_opt;
	}

	/**
	 * @param $new_bg_color
	 */
	public function set_banner_bg_color( $new_bg_color ) {
		$this->bg_color = $new_bg_color;
	}

	/**
	 * @param $new_display_pages
	 */
	public function set_banner_display_pages( $new_display_pages ) {
		$this->display_pages = $new_display_pages;
	}

	/**
	 * @param $new_display_cat
	 */
	public function set_banner_display_cat( $new_display_cat ) {
		$this->display_cat = $new_display_cat;
	}

	public function set_banner_display_home( $new_display_home ) {
		$this->display_home = $new_display_home;
	}

	public function set_banner_hide_btn( $new_hide_btn ) {
		$this->hide_btn = $new_hide_btn;
	}

	public function set_banner_timer_to_show( $new_timer_to_show ) {
		$this->timer_to_show = $new_timer_to_show;
	}

	public function save_banner() {
		$this->options->set_name( $this->name );
		$this->options->set_content( $this->content );
		$this->options->set_hide_opt( $this->hide_opt );
		$this->options->set_bg_color( $this->bg_color );
		$this->options->set_display_pages( $this->display_pages );
		$this->options->set_display_cat( $this->display_cat );
		$this->options->set_display_home( $this->display_home );
		$this->options->set_hide_btn( $this->hide_btn );
		$this->options->set_timer_to_show( $this->timer_to_show );

		$this->save_options();

	}

	/**
	 * @param $str
	 *
	 * @return string
	 */
	public function is_checked_hide_opt( $str ) {
		$ans = '';
		if ( strpos( $this->hide_opt, $str ) !== false ) {
			$ans = 'checked';
		}

		return $ans;
	}

	/**
	 * @param $array
	 * @param $val
	 *
	 * @return string
	 */
	public function is_checked_display_options( $array, $val ) {
		$ans = '';
		if ( in_array( $val, $array, false ) ) {
			return 'checked';
		}

		return $ans;
	}

	/**
	 * @return string
	 */
	public function display_options() {
		$ans        = '<div class="display-widgets">';
		$categories = get_categories( array(
			'orderby' => 'name',
			'order'   => 'ASC',
		) );
		$pages      = new WP_Query();
		$posts      = new WP_Query();
		$pages      = $pages->query( array(
			'post_type'      => 'page',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			'orderby'        => 'title',
		) );

		$ans .= '<h5>Основные</h5>';
		$ans .= '<label><input class="uk-checkbox" type="checkbox" name="all-pages" value=""><strong> Выбрать все</strong>';
		$ans .= '<label><input class="uk-checkbox" type="checkbox" name="home" value="'
		        . $this->display_home . '" '
		        . $this->is_checked_display_options( array( 'true' ), $this->display_home )
		        . '><strong> Главная</strong>';

		$ans .= '<h5>Страницы:</h5>';
		foreach ( $pages as $page ) {
			$ans .= '<label><input class="uk-checkbox" '
			        . 'type="checkbox" name="page" value="'
			        . $page->ID . '" '
			        . $this->is_checked_display_options( $this->get_banner_display_pages(), $page->ID )
			        . '>'
			        . $page->post_title
			        . '</label>';
		}
		wp_reset_postdata();

		$ans .= '<h5>Посты по категориям:</h5>';
		foreach ( $categories as $category ) {
			$ans .= '<label><input class="uk-checkbox" type="checkbox" name="category" value="'
			        . $category->term_id . '"><strong>'
			        . $category->name . '</strong></label>';

			$ans .= '<div class="posts-from-category-' . $category->term_id . '">';
			$ans .= '<label>&nbsp;<input class="uk-checkbox" type="checkbox" name="category_description" value="'
			        . $category->term_id . '" '
			        . $this->is_checked_display_options( $this->get_banner_display_cat(), $category->term_id )
			        . '><em class="uk-text-muted">На странице категории</em></label>';

			$cat_posts = $posts->query( array(
				'post_type'      => 'post',
				'cat'            => $category->term_id,
				'post_status'    => 'publish',
				'posts_per_page' => - 1,
				'orderby'        => 'title',
			) );

			foreach ( $cat_posts as $post ) {
				$ans .= '<label>&nbsp;&nbsp;<input class="uk-checkbox post-' . $post->ID
				        . '" type="checkbox" name="post" value="' . $post->ID . '" '
				        . $this->is_checked_display_options( $this->get_banner_display_pages(), $post->ID )
				        . '>' . $post->post_title . '</label>';

			}
			$ans .= '</div>';

			wp_reset_postdata();

		}

		$ans .= '</div>';

		return $ans;
	}

	/**
	 * @return string
	 */
	public function render_admin() {
		$output = '';
		ob_start();
		?>

		<h3 class="banner-<?php echo esc_attr( $this->id ); ?>"><?php echo esc_html( $this->get_banner_name() ); ?></h3>
		<div class="admin-banner-container banner-<?php echo esc_attr( $this->id ); ?> "
			 data-banner-id="<?php echo esc_attr( $this->id ); ?>">
			<form action="" method="get" name="banner-<?php echo esc_attr( $this->id ); ?>">
				<fieldset class="uk-fieldset">

					<legend class="uk-legend">Настройки баннера</legend>

					<div class="uk-margin">
						<label><input class="uk-input banner-name"
									  name="banner-name"
									  value="<?php echo esc_attr( $this->name ); ?>">
							<em class="uk-text-muted">Заголовок на сайте не выводится, только для удобного обозначение в
								админке.</em></label>
					</div>

					<p class="uk-margin uk-clearfix">
						<button class="uk-button uk-button-primary uk-button-small choose_template"
								data-banner-id="<?php echo esc_attr( $this->id ); ?>"
								uk-toggle="target: #template_modal_id-<?php echo esc_attr( $this->id ); ?>">Выбрать
							шаблон
						</button>
					</p>

					<div id="template_modal_id-<?php echo esc_attr( $this->id ); ?>" class="template_modal" uk-modal>
						<div class="uk-modal-dialog uk-modal-body">
							<h2 class="uk-modal-title">Выберите шаблон</h2>
							<div class="uk-margin template-1">
								<label>
									<textarea class="uk-textarea wysiwyg-textarea"
											  id="template_modal_content-<?php echo $this->id; ?>"
											  rows="5"
											  name="banner_content"><?php echo stripcslashes( $this->content ); ?>
									</textarea>
									<em class="uk-text-muted">Основной текст</em>
								</label>
							</div>
							<div class="uk-margin template-1">
								<label>
									<input class="uk-input " name="btn_content">
									<em class="uk-text-muted">Текст кнопки</em>
								</label>
							</div>
							<div class="uk-margin template-1">
								<label>
									<input class="uk-input" name="btn_link">
									<em class="uk-text-muted">Ссылка для кнопки (http://example.com)</em>
								</label>
							</div>
							<div class="uk-margin template-1">
								<label>
									<input class="uk-input uk-form-width-small" type="color" value="#fcad26"
										   name="btn_bg_color">
									<em class="uk-text-muted">Цвет фона кнопки</em>
								</label>
							</div>
							<div class="uk-margin template-1">
								<label>
									<input class="uk-input uk-form-width-small" type="color" value="#ffffff"
										   name="btn_text_color">
									<em class="uk-text-muted">Цвет текста кнопки</em>
								</label>
							</div>
							<button class="uk-modal-close uk-button uk-button-primary uk-button-small template_save"
									type="button">Сохранить
							</button>
							<button class="uk-modal-close uk-button uk-button-default uk-button-small" type="button">
								Отмена
							</button>
						</div>
					</div>

					<div class="uk-margin">
						<label><input class="uk-input uk-form-width-small"
									  type="color"
									  name="banner-bg-color"
									  value="<?php echo esc_attr( $this->bg_color ); ?>">
							<em class="uk-text-muted"> Цвет заднего фона</em></label>
					</div>

					<div class="uk-margin">
						<label><input class="uk-checkbox" type="checkbox" name="hide-btn"
									  value="<?php echo $this->hide_btn; ?>" <?php if ( $this->hide_btn ): echo "checked"; endif; ?>>
							<em class="uk-text-muted"> Кнопка сворачивания</em></label>
					</div>

					<div class="uk-margin">
						<label><input class="uk-input uk-form-width-small uk-form-small"
									  name="timer-to-show"
									  value="<?php echo esc_attr( $this->timer_to_show ); ?>">
							<em class="uk-text-muted"> Таймер задержки вывода в секундах</em></label>
					</div>

					<div class="uk-margin">

						<textarea class="uk-textarea banner-content-formatted wysiwyg-textarea"
								  id="banner_content-formatted-<?php echo $this->id; ?>"
								  rows="5"
								  placeholder="Контент"
								  name="banner-content-formatted"><?php echo stripcslashes( $this->content ); ?></textarea>
						<em class="uk-text-muted">Содержимое баннера.</em>
					</div>

					<div class="uk-margin">

						<textarea class="uk-textarea banner-content"
								  id="banner_content-<?php echo $this->id; ?>"
								  rows="5"
								  placeholder="Контент"
								  name="banner-content"><?php echo stripcslashes( $this->content ); ?></textarea>
						<em class="uk-text-muted">"Сырой" HTML.</em>
					</div>


					<div class="uk-margin uk-grid-medium uk-child-width-auto" uk-grid>
						<label><input class="uk-checkbox" type="checkbox" name="hide-xs"
									  value="hide-xs" <?php echo $this->is_checked_hide_opt( 'hide-xs' ); ?>>
							hide-xs</label>
						<label><input class="uk-checkbox" type="checkbox" name="hide-sm"
									  value="hide-sm" <?php echo $this->is_checked_hide_opt( 'hide-sm' ); ?>>
							hide-sm</label>
						<label><input class="uk-checkbox" type="checkbox" name="hide-md"
									  value="hide-md" <?php echo $this->is_checked_hide_opt( 'hide-md' ); ?>>
							hide-md</label>
						<label><input class="uk-checkbox" type="checkbox" name="hide-lg"
									  value="hide-lg" <?php echo $this->is_checked_hide_opt( 'hide-lg' ); ?>>
							hide-lg</label>
					</div>
					<div class="uk-margin uk-grid-small uk-child-width-auto" uk-grid>
						<p><em class="uk-text-muted">Прячет баннер при соответствующем разрешении:<br>
								hide-xs - до 768px | hide-sm - от 768px до 992px | hide-md - от 992px до 1200px |
								hide-lg - выше 1200px
							</em>
						</p>

					</div>


					<div class="uk-margin">
						<?php echo $this->display_options(); ?>

					</div>

					<p class="uk-margin uk-clearfix">
						<button class="uk-button uk-button-primary uk-button-small save-banner"
								data-banner-id="<?php echo esc_attr( $this->id ); ?>">Сохранить
						</button>
						<span>Сохранено</span>
						<button class="uk-button uk-button-text uk-text-danger uk-float-right remove-banner"
								data-banner-id="<?php echo esc_attr( $this->id ); ?>">Удалить
						</button>
					</p>
				</fieldset>
			</form>
		</div>

		<?php


		$output .= ob_get_clean();

		return $output;
	}

	/**
	 * @return string
	 */
	public function render_public() {
		$output = '';
		ob_start();
		echo $this->render_style_public();
		?>
		<div id="topper_banner-id-<?php echo $this->id; ?>"
			 class="wrap-topper-top uk-sticky <?php echo esc_attr( $this->hide_opt ); ?>"
			 data-timer-to-show=<?php echo esc_attr( $this->timer_to_show ); ?>>
			<div class="banner-content wrap">
				<?php echo stripcslashes( $this->content ); ?>
			</div>
		</div>
		<?php if ( $this->hide_btn ) : ?>
			<div class="topper-hide-btn uk-sticky <?php echo esc_attr( $this->hide_opt ); ?>">
				<i class="fa fa-angle-up fa-2x" aria-hidden="true"></i>
			</div>
		<?php endif; ?>


		<?php
		$output .= ob_get_clean();

		return $output;

	}

	private function render_style_public() {
		$output = '';
		ob_start();
		?>
		<style>
			.wrap-topper-top {
				background-color: <?php echo esc_attr( $this->get_banner_bg_color() ); ?>;
			}

			.topper-hide-btn {

				background-color: <?php echo esc_attr( $this->get_banner_bg_color() ); ?>;
				border-radius: 0 0 32px 32px;
				cursor: pointer;
			}
		</style>
		<?php
		$output .= ob_get_clean();

		return $output;
	}
}
